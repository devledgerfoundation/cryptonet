﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public float maxHealth;
    public float minHealth;
    //public float 

    public float enemyHealth;
    private float health;
    public float movementSpeed;

    public GameObject player;

    private bool triggeringPlayer;
    public bool aggro;

    public float attackTimer;
    private float _attackTimer;

    private bool attacked;

    public float maxDamage;
    public float minDamage;
    public float damage;


    private void Start()
    {
        player = GameObject.FindWithTag("Player");
        _attackTimer = attackTimer;
        health = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if (aggro){FollowPlayer();}
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player"){triggeringPlayer = true;}
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player"){triggeringPlayer = false;}
    }

    public void Attack()
    {
        if (!attacked)
        {
            damage = Random.Range(minDamage, maxDamage);
            player.GetComponent<Player>().health -= damage;
            attacked = true;
            print("Enemy Attacked player");
        }
    }

    public void FollowPlayer()
    {
        if (!triggeringPlayer)
        {
            this.transform.position = Vector3.MoveTowards(transform.position, player.transform.position, movementSpeed);
        }
        if (_attackTimer <= 0){attacked = false; _attackTimer = attackTimer; }

        if (attacked){_attackTimer -= 1 * Time.deltaTime;}

        //if (!attacked) { _attackTimer = attackTimer; }
        Attack();
    }
}
